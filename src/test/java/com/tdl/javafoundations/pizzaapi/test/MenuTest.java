package com.tdl.javafoundations.pizzaapi.test;

import com.tdl.javafoundations.pizzaapi.Menu;
import com.tdl.javafoundations.pizzaapi.MenuItem;
import org.junit.Test;

public class MenuTest {
    @Test
    public void menu_items_test() {
        MenuItem expected = new MenuItem();

        Menu menu = new Menu();

        assert menu.getItems().get(0).deepequals(expected);
    }
}
