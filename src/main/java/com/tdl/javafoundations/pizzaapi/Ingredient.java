package com.tdl.javafoundations.pizzaapi;

public class Ingredient implements Priceable, ProductInfo {

    private int price;
    private String productInfo;

    public Ingredient(String productInfo, int price){
        this.productInfo = productInfo;
        this.price = price;
    }

    @Override
    public String getProductInfo() {
        return productInfo;
    }

    @Override
    public int getPrice() {
        return price;
    }
}
