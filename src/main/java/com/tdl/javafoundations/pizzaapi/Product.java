package com.tdl.javafoundations.pizzaapi;

public class Product implements Priceable, ProductInfo{

    private int price;
    private String productInfo;

    public Product(String productInfo, int price){
        this.productInfo = productInfo;
        this.price = price;
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public String getProductInfo() {
        return productInfo;
    }
}
