package com.tdl.javafoundations.pizzaapi;

public class BasketItem implements Priceable{

    private int price;
    private int quantity;
    private MenuItem menuItem;

    @Override
    public int getPrice() {
        return price;
    }

    public MenuItem getMenuItem() {
        return menuItem;
    }

    public int getQuantity() {
        return quantity;
    }
}
