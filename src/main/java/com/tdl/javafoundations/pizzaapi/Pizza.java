package com.tdl.javafoundations.pizzaapi;

import java.util.*;

public class Pizza implements ProductInfo, Priceable, Customizable {

    private List<Ingredient> ingredientList = new ArrayList<Ingredient>();
    private int price;
    private String productInfo;

    public Pizza(String productInfo, int price) {
        this.productInfo = productInfo;
        this.price = price;
    }

    @Override
    public void addIngredient(Ingredient ingredient) {
        ingredientList.add(ingredient);
    }

    @Override
    public void removeIngredient(Ingredient ingredient) {
        ingredientList.remove(ingredient);
    }

    @Override
    public List<Ingredient> getIngredients() {
        return ingredientList;
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public String getProductInfo() {
        return productInfo;
    }
}
