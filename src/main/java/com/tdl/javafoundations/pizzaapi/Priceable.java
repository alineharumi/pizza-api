package com.tdl.javafoundations.pizzaapi;

public interface Priceable {

    int getPrice();
}
